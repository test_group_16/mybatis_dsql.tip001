package mybatis_dsql.tip001;

import java.sql.SQLType;
import java.sql.JDBCType;
import org.mybatis.dynamic.sql.SqlTable;
import org.mybatis.dynamic.sql.SqlColumn;
import java.util.Date;
import java.math.BigDecimal;

public final class Support {

    public static final Member Member = new Member();

    public static final SqlColumn <Integer> id = Member.id;
    public static final SqlColumn <String> chr01 = Member.chr01;
    public static final SqlColumn <BigDecimal> num01 = Member.num01;
    public static final SqlColumn <Date> dat01 = Member.dat01;

    public static final class Member extends SqlTable {
        public final SqlColumn <Integer> id = column("ID", JDBCType.INTEGER);
        public final SqlColumn <String> chr01 = column("CHR01", JDBCType.VARCHAR);
        public final SqlColumn <BigDecimal> num01 = column("NUM01", JDBCType.DECIMAL);
        public final SqlColumn <Date> dat01 = column("DAT01", JDBCType.DATE);

        public Member() {
            super("TIP001");
        }
    }

}