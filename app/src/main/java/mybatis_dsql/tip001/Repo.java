package mybatis_dsql.tip001;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.builder.annotation.ProviderMethodResolver;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.apache.ibatis.jdbc.SQL;
import java.sql.JDBCType;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;

@Mapper
public interface Repo {

    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    public List<Map<String, Object>> selectAll(SelectStatementProvider selectStatement);


//    // @Select("select * from tip001")
//    @SelectProvider(TodoSqlProvider.class)
//    public List<Map<String, Object>> selectAll();
//
//    class TodoSqlProvider implements ProviderMethodResolver {
//        public String selectAll() {
//            // return new SQL() {{
//            //     SELECT("*");
//            //     FROM("tip001");
//            // }}.toString();
//            return """
//                select * from tip001
//            """;
//        }
//    }
}